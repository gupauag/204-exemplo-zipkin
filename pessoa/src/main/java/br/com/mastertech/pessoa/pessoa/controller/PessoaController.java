package br.com.mastertech.pessoa.pessoa.controller;

import br.com.mastertech.pessoa.pessoa.clients.CarroClient;
import br.com.mastertech.pessoa.pessoa.clients.Cep;
import br.com.mastertech.pessoa.pessoa.clients.CepClient;
import br.com.mastertech.pessoa.pessoa.model.Carro;
import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PessoaController {

    @Autowired
    private CarroClient carroClient;

    @Autowired
    private CepClient cepClient;

    @GetMapping("/{nome}/{placa}")
    public Pessoa create(@PathVariable String nome, @PathVariable String placa) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome(nome);

        Carro carroByPlaca = carroClient.getCarroByPlaca(placa);

        pessoa.setCarro(carroByPlaca);

        return pessoa;
    }

    @GetMapping("/{cep}")
    public Cep getCep(@PathVariable String cep) {
        try {
            return cepClient.getByCep(cep);
        } catch (FeignException.BadRequest e) {
            return new Cep();
        }
    }


}
